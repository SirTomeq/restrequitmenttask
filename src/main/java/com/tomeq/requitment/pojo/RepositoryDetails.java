package com.tomeq.requitment.pojo;

/**
 * Created by tomas on 25.04.2018.
 */
public class RepositoryDetails {
    private String fullName;
    private String description;
    private String cloneUrl;
    private Integer stars;
    private String createdAt;

    public RepositoryDetails(){}

    public RepositoryDetails(String fullName, String description, String cloneUrl, Integer stars, String createdAt){
        this.fullName = fullName;
        this.description = description;
        this.cloneUrl = cloneUrl;
        this.stars = stars;
        this.createdAt = createdAt;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
