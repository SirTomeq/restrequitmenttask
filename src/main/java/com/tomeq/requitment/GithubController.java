package com.tomeq.requitment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomeq.requitment.pojo.RepositoryDetails;
import com.tomeq.requitment.exception.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by tomas on 24.04.2018.
 */
@RestController
public class GithubController {

    public static final String GITHUB_API_URL = "https://api.github.com";
    public static final String RESOURCE_NOT_FOUND_FORMAT = "Resource %s/%s not found";

    @RequestMapping("/repositories/{owner}/{repository-name}")
    public String getRepositoryDetail(@PathVariable("owner") String owner, @PathVariable("repository-name") String repositoryName) throws NotFoundException, IOException {
        String url = String.format("%s/repos/%s/%s", GITHUB_API_URL, owner, repositoryName);

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());

            JsonNode fullName = root.path("full_name");
            JsonNode description = root.path("description");
            JsonNode cloneUrl = root.path("clone_url");
            JsonNode stars = root.path("stargazers_count");
            JsonNode createdAt = root.path("created_at");

            RepositoryDetails repositoryDetails =
                    new RepositoryDetails(fullName.textValue(), description.textValue(),
                            cloneUrl.textValue(), stars.intValue(), createdAt.textValue());

            return mapper.writeValueAsString(repositoryDetails);
        } catch (HttpClientErrorException e) {
            throw new NotFoundException(String.format(RESOURCE_NOT_FOUND_FORMAT, owner, repositoryName));
        }
    }
}
