package com.tomeq.requitment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Created by tomas on 27.04.2018.
 */
public class NotFoundException extends HttpClientErrorException {

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
