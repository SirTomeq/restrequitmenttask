package com.tomeq.requitment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequitmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequitmentApplication.class, args);
	}
}
