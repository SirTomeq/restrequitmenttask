package com.tomeq.requitment;

import com.tomeq.requitment.exception.NotFoundException;
import com.tomeq.requitment.pojo.RepositoryDetails;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertTrue;

/**
 * Created by tomas on 25.04.2018.
 */
@SpringBootTest
public class GithubControllerTests {
    @Test
    public void getRepositoryDetailTest_trueWhenValidParams() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/repositories/zaach/jison";
        ResponseEntity<RepositoryDetails> response = restTemplate.getForEntity(url, RepositoryDetails.class);

        assertTrue(response.getStatusCode().equals(HttpStatus.OK));
        assertTrue("zaach/jison".equals(response.getBody().getFullName()));
    }

    @Test
    public void getRepositoryDetailTest_failWhenNoOwnerAndRepo() {
        String owner = "TomeqDev";
        String repoName = "abc";
        assertThatThrownBy(() -> new GithubController().getRepositoryDetail(owner, repoName))
                .isInstanceOf(NotFoundException.class)
                .hasMessage(String.format(
                        HttpStatus.NOT_FOUND + " " + GithubController.RESOURCE_NOT_FOUND_FORMAT, owner, repoName));
    }
}
